/*
 * Aplicativo para extrair dados do Huawei "Health"
 * SDK HiHealth kit
 * Copyright (c) Philo Care do Brasil Ltda. Todos direitos reservados
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2019. All rights reserved.
 */

package com.philo.hub;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;

import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.huawei.hihealth.device.HiHealthDeviceInfo;
import com.huawei.hihealth.listener.ResultCallback;
import com.huawei.hihealthkit.data.HiHealthData;
import com.huawei.hihealthkit.data.HiHealthSequenceData;
import com.huawei.hihealthkit.data.store.HiHealthDataStore;
import com.huawei.hihealthkit.data.store.HiRealTimeListener;

import java.io.IOException;
import java.text.ParseException;
import java.util.Timer;
import java.util.TimerTask;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private static final int GET_GENDER = 2;

    private static final int GET_BIRTHDAY = 3;

    private static final int START_READING_HEARTRATE = 12;
    private static final int STOP_READING_HEARTRATE = 13;

    private TextView dataText;
    private TextView textConnection;
    private PopupWindow window;
    public int counter=0;

    // serviço que será executado em background
    //private GetDataService mGetDataService;

    private Context ctx;
    public Context getCtx() {
        return ctx;
    }

    // Nome do canal de comunicação para mensagens no status bar
    public static final String CHANNEL_ID = "common";
    public static final String ACTION_SNOOZE = "snooze";
    public static final String ACTION_MONITOR = "monitoring_action";
    private Notification note;

    // controle de indexação dos arquivos (evita perda de dados)
    public static int index_save = 0;

    public static String user_id;

    // TextView for displaying operation information on the UI
    private TextView logInfoView;
    private TextView tv1;
    private TextView tv2;
    private TextView tv3;
    private TextView mTitleTextView;
    private Message msg;

    //#####################################################################
    // Mensagens
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String s = msg.obj.toString();
//            Toast.makeText(mContext, s, Toast.LENGTH_LONG).show();

            switch (msg.what) {
                case 1:
                    tv1.setText(s + "\n");
                    break;
                case 2:
                    tv2.setText(s + "\n");
                    break;
                case 3:
                    tv3.setText(s + "\n");
                    break;
            }
        }
    };

    private void sendMsg(String message, int what) {
        msg = Message.obtain();
        msg.what = what;
        msg.obj = message;
        mHandler.sendMessage(msg);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //dataText = findViewById(R.id.dataText);

        ctx = this;

        logInfoView = (TextView) findViewById(R.id.data_controller_log_info);
        logInfoView.setMovementMethod(ScrollingMovementMethod.getInstance());

        // Exibição de textos
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);

        requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,READ_EXTERNAL_STORAGE}, 1);

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //popSplashWindow(findViewById(R.id.main_layout), R.layout.activity_splash);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        View main_view = (View)findViewById(R.id.main_layout);
                        main_view.setVisibility(View.VISIBLE);
                        //dismissSplashWindow();
                    }
                }, 2000);
            }
        }, 100);*/

        initView();

        /*
        Notificações no status bar
        */
        createNotificationChannel();

        // Verifica a permissão do aplicativo
        doGetGender(getCtx());

        /*mGetDataServiceIntent = new Intent(this, GetDataService.class);
        mGetDataServiceIntent.putExtra("inputExtra", "Foreground Service Example in Android");
        ContextCompat.startForegroundService(this, mGetDataServiceIntent);*/

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_MONITOR);
        filter.addAction(ACTION_SNOOZE);

        // serviços de background
        //Intent mGetDataServiceIntent;
        //Intent serviceIntent;
        BcastReceiver receiver = new BcastReceiver();
        receiver.setContext(getCtx());
        registerReceiver(receiver, filter);

        // Controle de index files
        String str_index_save = SysUtils.loadInfoData(ctx, "index");

        if (str_index_save.equals(SysUtils.NOT_FOUND) || str_index_save.equals("")) {
            index_save = 0;
        } else {
            index_save = Integer.parseInt(str_index_save);
        }

        // por enquanto o identificador será o Mc address do bluetooth
        // user_id = "50:48:49:4C:4F:01";
        // 24-07-2020 - Robson
        // Por decisão do Douglas não vamos focar no serviço e o plicativo irá fazer a
        // extração de dados e exibição na tela
        // Cria o serviço "imortal" de busca de dados
        //mGetDataService = new GetDataService();
        //mGetDataService.setCtx(getCtx());
        //mGetDataServiceIntent = new Intent(getCtx(), mGetDataService.getClass());
        //startService(mGetDataServiceIntent);
        /*
        if (!isMyServiceRunning(mGetDataService.getClass())) {
            //startService(mGetDataServiceIntent);
        }
         */
        // local para salvar o código da pulseira selecionada
        user_id = SysUtils.getSavedDevice(this);

        if (user_id.equals("")) {
            // Não tem usuário ainda, não iniciliza o timer
        } else {
            // inicia o timer resposável pela extração de dados
            //startTimer();
        }
        // inicializa o controlador dos dados
        HuaweiDataColector.initDataController(getCtx(), logInfoView);

        // inicia o timer resposável pela extração de dados
        //startTimer();
    }

    private TimerTask timerTask;
    long oldTime=0;

    /**
    Inicializa o timer de extração de dados do Huawei Health
     */
    public void startTimer() {
        //set a new Timer
        /**
         Variáveis para o timer de extração de dados
         */
        Timer timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask(ctx);

        //schedule the timer, to wake up every 1 second
        timer.schedule(timerTask, 5000, 500000); //
    }

    /**
     * Inicializa a tarefa (thread) que faz o serviço de extração e calculos
     */
    public void initializeTimerTask(Context ctx) {
        timerTask = new TimerTask() {
            public void run() {
                Log.i("in timer", "in timer ++++  "+ (counter++));
                try {
                    // Mostra notificação de monitoramento
                    showMonitorNote();

                    // Verifica a permissão e conexão do aplicativo e exibe situação atual
                    doGetGender(getCtx());

                    // verifica se tem data de ultima leiura HR
                    /*if (getLastHRRead()) {
                        // se tem usar para buscar somente a diferença
                        doGetHRData();
                    } else {
                        // não tem arquivo, busca 30 dias
                        doExecQueryHR30Days(getCtx());
                    }*//*
                    // verifica se tem data de ultima leiura Sleep
                    if (getLastSleepRead()) {
                        // se tem usar para buscar somente a diferença
                        doGetSleepData();
                    } else {
                        // não tem arquivo, busca 30 dias
                        doExecQuerySleep30Days(getCtx());
                    }*/

                    // transfere arquivos para o servidor PhiloCare.com
                    SysUtils.doSendFiles(getCtx());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    /**
     * Get data from Huawei
     * @param view linked view
     * @throws ParseException
     * @throws IOException
     */
    public void readData(View view) throws ParseException, IOException {

        // busca dados do Saúde
        HuaweiDataColector.getHealthData(getCtx());
    }

    /**
     * Verifica se já possui ultima leitura (o registro salvo de HR)
     * Sem registro significa que será a primeira vez que está executando
     */
    /*private boolean getLastHRRead() {
        // verifica se o arquivo existe
        String res = SysUtils.getHRSavedDate(getCtx());
        if (res.equals(SysUtils.NOT_FOUND)) {
            return false;
        } else {
            return true;
        }
    }*/
    /**
     * Verifica se já possui ultima leitura (o registro salvo de Sleep)
     * Sem registro significa que será a primeira vez que está executando
     */
    /*private boolean getLastSleepRead() {
        // verifica se o arquivo existe
        String res = SysUtils.getSleepSavedDate(getCtx());
        if (res.equals(SysUtils.NOT_FOUND)) {
            return false;
        } else {
            return true;
        }
    }*/

    /**
     * Notificação de monitoramento
     */
    private void showMonitorNote() {
        Intent intent = new Intent(this, BcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                this.getApplicationContext(), 234324243, intent, 0);

        note = new Notification(this, MainActivity.class);
        note.send_note("Oi", "Philo Care Hub", "Estamos monitorando sua saúde!", R.drawable.ic_stat_philo, pendingIntent);
    }

    /**
     * Extrai os dados do aplicativo Huawei Sleep
     */
    /*private void doGetSleepData() throws ParseException {
        // pega a última data de extração
        String last_datetime = SysUtils.getSleepSavedDate(getCtx());
        // pega a data de hoje
        Date todayTime = new Date();
        SimpleDateFormat sdf;
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date lastDay = sdf.parse(last_datetime);
        // compara o dia para verificar se o último dia extraido é hoje
        if (todayTime.getDay() == lastDay.getDay()) {
            // os dias são iguais, não faz nada
        } else {
            // procede extração à partir do último dia
            doGetSleepLastData(lastDay);
        }
        //long startTime = endTime - 1000L * 60 * 60 * 24 * 5; // Check Data of the latest 1 days
    }*/

    /**
     * Extrai os dados do aplicativo Huawei HR
     */
    /*private void doGetHRData() throws ParseException {
        // pega a última data de extração
        String last_datetime = SysUtils.getHRSavedDate(getCtx());
        // pega a data de hoje
        Date todayTime = new Date();
        SimpleDateFormat sdf;
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date lastDay = sdf.parse(last_datetime);
        // compara o dia para verificar se o último dia extraido é hoje
        if (todayTime.getDay() == lastDay.getDay()) {
            // os dias são iguais, não faz nada
        } else {
            // procede extração à partir do último dia
            doGetHRLastData(lastDay);
        }
        //long startTime = endTime - 1000L * 60 * 60 * 24 * 5; // Check Data of the latest 1 days
    }*/

    /**
     * Extrai dados a partir de uma data e coloca em arquivo
     */
    /*private void doGetHRLastData(Date lastDay) {
        int timeout = 0;
        // o fim é o dia de hoje
        long endTime = System.currentTimeMillis();
        // o ultimo dia extraido
        long startTime = lastDay.getTime(); // Check Data of the last 30 days

        HiHealthDataQuery hiHealthDataQuery = new HiHealthDataQuery(
                HiHealthSetType.DATA_SET_HEART,
                startTime, //<----- variable startTime used
                endTime,//<----- variable endTime used
                new HiHealthDataQueryOption()
        );

        HiHealthDataStore.execQuery(getCtx(), hiHealthDataQuery, timeout, new ResultCallback() {
            @Override
            public void onResult(int resultCode, Object data) {
                if (data != null) {
                    Log.i(TAG, "query not null");
                    List dataList = (List) data;
                    long lastDay = 0;
                    StringBuffer stringBuffer = new StringBuffer();
                    // verifica se teve sucesso
                    if (resultCode == 0) {
                        long last_day = 0;
                        //stringBuffer.append("resultCode is ").append(String.valueOf(resultCode)).append(";(");
                        for (Object obj : dataList) {
                            HiHealthSetData hiHealthDatas = (HiHealthSetData) obj;
                            stringBuffer.append("#DataType: ").append(String.valueOf(hiHealthDatas.getType())).append(";");
                            Map map = hiHealthDatas.getMap();
                            long day = hiHealthDatas.getStartTime();
                            for (Object key : map.keySet()) {
                                // colocar timestamp nos dados
                                stringBuffer.append(key).append(":");
                                Object value = map.get(key);
                                stringBuffer.append(value).append(" ");
                                // código da data
                                if (last_day < day) {
                                    lastDay = day;
                                }
                            }
                        }
                    } else {
                        stringBuffer.append("Error!");
                    }
                    Log.i(TAG, stringBuffer.toString());
                    // salva os dados em arquivo
                    SysUtils.writeTxtToFile(stringBuffer.toString(), SysUtils.pathConfig, SysUtils.dataHRFileName);
                    // salva a última data obtida
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String lastts = sdf.format(new Date(lastDay));
                    try {
                        SysUtils.saveHRLastDate(getApplicationContext(), lastts);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }*/

    // 24-07-2020 - Robson
    // Por decisão do Douglas não vamos focar no serviço e o plicativo irá fazer a
    // extração de dados e exibição na tela
    /**
    Verifica se o serviço está rodadando
     */
    /*private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }*/

    //#############################################################################################
    // Funções executadas depois do oncreate
    //#############################################################################################
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume () {
        super.onResume();
        // usaremos a função getGender para verificar se estamos conseguido ler o Huawei
        doGetGender(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }


    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "onStop");

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
        // 24-07-2020 - Robson
        // Por decisão do Douglas não vamos focar no serviço e o plicativo irá fazer a
        // extração de dados e exibição na tela
        //sendBroadcast(new Intent("RESTART"));
        //stopService(mGetDataServiceIntent);
    }


    /**
    Cria o canal de comunicação do aplicativo
    */
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }
    }

    /**
    Desativa a tela de splash
     */
    public void dismissSplashWindow() {
        if (window != null) {
            window.dismiss();
            window = null;

        }

    }

    /**
    Exibe a tela de splash
     */
    public void popSplashWindow(View parent, int windowRes) {
        if (window == null) {
            LayoutInflater lay = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert lay != null;
            View popView = lay.inflate(windowRes, null);
            popView.setAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_out));

            window = new PopupWindow(popView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, true);
            window.setOutsideTouchable(true);
            window.setFocusable(true);
            window.update();

            window.showAtLocation(parent, Gravity.CENTER_VERTICAL, 0, 0);
            window.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    //bStart = true;
                    //callRemoteScanDevice();
                    window = null;
                }
            });

            // Evento de toque na tela
            /*window.setTouchInterceptor(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    dismissSplashWindow();
                    return false;
                }
            });*/

        }
    }


    public String getAppVersion(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            String version = info.versionName;
            return version;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Inicializa os componentes da UI
     */
    private void initView() {
        //mSwipeRefreshLayout = (SwipeRefreshLayout) super.findViewById(R.id.mian_swipeRefreshLayout);
        //mRecyclerView = (RecyclerView) super.findViewById(R.id.main_recylerlist);
        mTitleTextView = (TextView) super.findViewById(R.id.main_title);
        mTitleTextView.setText(getString(R.string.app_name) + " " + getAppVersion(getCtx()));
        //mInfoTextView = (TextView) super.findViewById(R.id.textViewInfo);


        //textConnection = findViewById(R.id.tvConnection);

        /*Button buttonGetCount = findViewById(R.id.getCount);
        buttonGetCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //doExecQuerySleep30Days(MainActivity.this);
            }
            });
        //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
        //        .setAction("Action", null).show();


        Button buttonExecQuery = findViewById(R.id.execQuery);
        buttonExecQuery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //doExecQueryHR30Days(getCtx());
                //doExecQuery(MainActivity.this);
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
            }
        });

        Button buttonStartReadingHeartRate = findViewById(R.id.startReadingHeartRate);
        buttonStartReadingHeartRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SysUtils.doSendFiles(getCtx());
                //doStartReadingHeartRate(MainActivity.this);
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
            }
        });

        Button buttonStopReadingHeartRate = findViewById(R.id.stopReadingHeartRate);
        buttonStopReadingHeartRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doStopReadingHeartRate(MainActivity.this);
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
            }
        });

        Button buttonGetBirthday = findViewById(R.id.getBirthday);
        buttonGetBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //doGetLastDate(MainActivity.this);
            }
        });*/


    }


    //#############################################################################################
    // AÇÕES DE BOTÕES
    //#############################################################################################
    /**
    Envia um broadcast para o receiver
     */
    public void btnBroadcastClick(View view) {

        Intent intent = new Intent(this, BcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                this.getApplicationContext(), 234324243, intent, 0);

        note = new Notification(this, MainActivity.class);
        note.send_note("Oi", "Philo Care Hub", "Estamos monitorando sua saúde!", R.drawable.ic_stat_philo, pendingIntent);


        /*
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        assert alarmManager != null;
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                + (5 * 1000), pendingIntent);
        Toast.makeText(this, "Alarm set in " + 5 + " seconds",
                Toast.LENGTH_LONG).show();
         */
    }

    /*
    Ativa a tela de configuração
    */
    public void btnSettingsClick(View view) {
        Intent intent = new Intent(
                MainActivity.this, SettingsActivity.class
        );
        startActivity(intent);
    }

    /*
    Ativa a tela de autenticação
    */
    public void btnAuthClick(View view) {
        Intent intent = new Intent(
                MainActivity.this, HuaweiAuthActivity.class
        );
        startActivity(intent);
    }


    /*
    ################################################################################################
    Métodos de acesso ao "Saúde" da Huawei
    ################################################################################################
     */

    /**
     * Developer Alliance Code:getGender
     *
     * @param context context
     */
    private void doGetGender(Context context) {
        HiHealthDataStore.getGender(context, new ResultCallback() {
            @Override
            public void onResult(int errorCode, Object gender) {
                Log.i(TAG, "call doGetGender() resultCode is " + errorCode);
                Log.i(TAG, "call doGetGender() gender is " + gender);
                if (gender != null) {
                    Message message = Message.obtain();
                    message.what = GET_GENDER;
                    message.obj = gender;
                    String res = String.valueOf(gender);
                    if (res.equals("failed")) {
                        sendMsg(getString(R.string.fail_auth), 1);
                    } else {
                        sendMsg(getString(R.string.connected), 1);
                    }
                } else {
                    sendMsg(getString(R.string.fail_huawei), 1);
                }
            }
        });
    }

    /**
     * Developer Alliance Code:getBirthday
     *
     * @param context context
     */
    private void doGetBirthday(Context context) {
        HiHealthDataStore.getBirthday(context, new ResultCallback() {
            @Override
            public void onResult(int resultCode, Object birthday) {
                Log.i(TAG, "call doGetBirthday() resultCode is " + resultCode);
                Log.i(TAG, "call doGetBirthday() birthday is " + birthday);
                if (birthday != null) {
                    // For example, "1978-05-20" would return 19780520
                    Message message = Message.obtain();
                    message.what = GET_BIRTHDAY;
                    message.obj = birthday;
                }
            }
        });
    }

    /*private void doGetLastDate(Context context) {
        String last_hr_date;
        String last_sleep_date;

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        last_hr_date = SysUtils.getHRSavedDate(context);
        last_sleep_date = SysUtils.getSleepSavedDate(context);

        String macAddress = android.provider.Settings.Secure.getString(context.getContentResolver(), "bluetooth_address");

        dataText.setText("Última data HR:" + last_hr_date + " Última data Sleep: " + last_sleep_date +
                " BlueMAC: " + bluetoothAdapter.getAddress());
    }*/

    /**
     * Executa query para pegar dados de sono nos últimos 30 dias
     * Deve ser executada na primeira vez em que o aplicativo executa
     *
     */
    /*private void doExecQuerySleep30Days(Context context) {
        int timeout = 0;
        // o fim é o dia de hoje
        long endTime = System.currentTimeMillis();
        //long startTime = endTime - 1000L * 60 * 60 * 24 * 5; // Check Data of the latest 1 days
        // dia de hoje menos a 30 dias
        long startTime = endTime - 1000L * 60 * 60 * 24 * 30; // Check Data of the last 30 days

        HiHealthDataQuery hiHealthDataQuery = new HiHealthDataQuery(
                HiHealthSetType.DATA_SET_CORE_SLEEP,
                startTime, //<----- variable startTime used
                endTime,//<----- variable endTime used
                new HiHealthDataQueryOption()
        );

        //hiHealthDataQuery.setSampleType(HiHealthSetType.DATA_SET_CORE_SLEEP);
        HiHealthDataStore.execQuery(context, hiHealthDataQuery, timeout, new ResultCallback() {
            @Override
            public void onResult(int resultCode, Object data) {
                if (data != null) {
                    Log.i(TAG, "query not null");
                    List dataList = (List) data;
                    long lastDay = 0;
                    StringBuffer stringBuffer = new StringBuffer();
                    // verifica se teve sucesso
                    if (resultCode == 0) {
                        //stringBuffer.append("resultCode is ").append(String.valueOf(resultCode)).append(";(");
                        for (Object obj : dataList) {
                            HiHealthSetData hiHealthDatas = (HiHealthSetData) obj;
                            stringBuffer.append("#DataType: ").append(String.valueOf(hiHealthDatas.getType())).append(";");
                            Map map = hiHealthDatas.getMap();
                            for (Object key : map.keySet()) {
                                stringBuffer.append(key).append(":");
                                Object value = map.get(key);
                                stringBuffer.append(value).append(" ");
                                // código da data
                                if ((int)key == 44202) {
                                    lastDay = (long)value;
                                }
                            }
                        }
                    } else {
                        stringBuffer.append("Error!");
                    }
                    Log.i(TAG, stringBuffer.toString());
                    // salva os dados em arquivo
                    SysUtils.writeTxtToFile(stringBuffer.toString(), SysUtils.pathConfig, SysUtils.dataSleepFileName);
                    // salva a última data obtida
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String now = sdf.format(new Date(lastDay));
                    try {
                        SysUtils.saveSleepLastDate(getApplicationContext(), now);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }*/

    /**
     * Executa query para pegar dados de sono nos últimos 30 dias
     * Deve ser executada na primeira vez em que o aplicativo executa
     *
     */
    /*private void doExecQueryHR30Days(Context context) {
        int timeout = 0;
        // o fim é o dia de hoje
        long endTime = System.currentTimeMillis();
        //long startTime = endTime - 1000L * 60 * 60 * 24 * 5; // Check Data of the latest 1 days
        // dia de hoje menos a 30 dias
        long startTime = endTime - 1000L * 60 * 60 * 24 * 30; // Check Data of the last 30 days

        HiHealthDataQuery hiHealthDataQuery = new HiHealthDataQuery(
                HiHealthSetType.DATA_SET_HEART,
                startTime, //<----- variable startTime used
                endTime,//<----- variable endTime used
                new HiHealthDataQueryOption()
        );

        //hiHealthDataQuery.setSampleType(HiHealthSetType.DATA_SET_CORE_SLEEP);
        HiHealthDataStore.execQuery(context, hiHealthDataQuery, timeout, new ResultCallback() {
            @Override
            public void onResult(int resultCode, Object data) {
                if (data != null) {
                    Log.i(TAG, "query not null");
                    List dataList = (List) data;
                    long lastDay = 0;
                    StringBuffer stringBuffer = new StringBuffer();
                    // verifica se teve sucesso
                    if (resultCode == 0) {
                        long last_day = 0;
                        //stringBuffer.append("resultCode is ").append(String.valueOf(resultCode)).append(";(");
                        for (Object obj : dataList) {
                            HiHealthSetData hiHealthDatas = (HiHealthSetData) obj;
                            stringBuffer.append("#DataType: ").append(String.valueOf(hiHealthDatas.getType())).append(";");
                            Map map = hiHealthDatas.getMap();
                            long day = hiHealthDatas.getStartTime();
                            for (Object key : map.keySet()) {
                                // colocar timestamp nos dados
                                stringBuffer.append(key).append(":");
                                Object value = map.get(key);
                                stringBuffer.append(value).append(" ");
                                // código da data
                                if (last_day < day) {
                                    lastDay = day;
                                }
                            }
                        }
                    } else {
                        stringBuffer.append("Error!");
                    }
                    Log.i(TAG, stringBuffer.toString());
                    // salva os dados em arquivo
                    SysUtils.writeTxtToFile(stringBuffer.toString(), SysUtils.pathConfig, SysUtils.dataHRFileName);
                    // salva a última data obtida
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String lastts = sdf.format(new Date(lastDay));
                    try {
                        SysUtils.saveHRLastDate(getApplicationContext(), lastts);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }*/

    /**
     * Developer Alliance Code:getCount
     *
     * @param context context
     */
    private void doGetCount(final Context context) {
        final int maxCount = 500;
        final long endTime = System.currentTimeMillis();
        final long startTime = endTime - 1000L * 60 * 60 * 24 * 5; // Check Data of the latest 5 days
        //HiHealthDataQuery hiHealthDataQuery = new HiHealthDataQuery(HiHealthPointType.DATA_POINT_STEP_SUM, startTime,
        //        endTime, new HiHealthDataQueryOption());

        HiHealthDeviceInfo sourceDev;// = new HiHealthDeviceInfo("acha", "Honor", "Smartband");
        HiHealthData dataDev = new HiHealthSequenceData();

        dataDev.setStartTime(startTime);
        dataDev.setEndTime(endTime);
        dataDev.setType(0);

        sourceDev = dataDev.getSourceDevice();

        //HiHealthSequenceData hiHealthSequenceData = new HiHealthSequenceData();

        //HiHealthDeviceInfo sourceDevice = hiHealthSequenceData.getSourceDevice();

        dataText.setText(sourceDev.getDeviceName());

        /*Log.i(TAG, "sampletype = " + hiHealthDataQuery.getSampleType());
        HiHealthDataStore.getCount(context, hiHealthDataQuery, new ResultCallback() {
            @Override
            public void onResult(int resultCode, Object data) {
                Log.i(TAG, "call doGetCount() resultCode is " + resultCode);
                Log.i(TAG, "call doGetCount() data is " + data);
                if (data != null) {
                    int count = (int) data;
                    Message message = Message.obtain();
                    message.what = GET_COUNT;
                    message.obj = count;
                    //demoHandler.sendMessage(message);
                    if (count > maxCount) {
                        Log.i(TAG, "call execQuery() with a shorten duration.");
                        long newEndTime = startTime + (endTime - startTime) / 2;
                        final HiHealthDataQuery query = new HiHealthDataQuery(HiHealthPointType.DATA_POINT_STEP_SUM,
                                startTime, newEndTime, new HiHealthDataQueryOption());
                        HiHealthDataStore.execQuery(context, query, 0, new ResultCallback() {
                            @Override
                            public void onResult(int resultCode, Object data) {
                                Log.i(TAG, "call execQuery() resultCode is " + resultCode);
                            }
                        });
                    }
                } else {
                    Message message = Message.obtain();
                    message.what = GET_COUNT;
                    message.obj = resultCode;
                    //demoHandler.sendMessage(message);
                }
            }
        });*/
    }


    /**
     * Developer Alliance Code:startReadingHeartRate
     *
     * @param context context
     */
    private void doStartReadingHeartRate(Context context) {
        HiHealthDataStore.startReadingHeartRate(context, new HiRealTimeListener() {
            @Override
            public void onResult(int resultCode) {
                Log.i(TAG, "call doStartReadingHeartRate() resultCode is " + resultCode);
                Message message = Message.obtain();
                message.what = START_READING_HEARTRATE;
                message.obj = resultCode;
                //demoHandler.sendMessage(message);
            }

            @Override
            public void onChange(int resultCode, String data) {
                Log.i(TAG, "call doStartReadingHeartRate() resultCode is " + resultCode);
                Log.i(TAG, "call doStartReadingHeartRate() value is " + data);
                Message message = Message.obtain();
                message.what = START_READING_HEARTRATE;
                message.obj = data;
                //demoHandler.sendMessage(message);
            }
        });
    }

    /**
     * Developer Alliance Code:stopReadingHeartRate
     *
     * @param context context
     */
    private void doStopReadingHeartRate(Context context) {
        HiHealthDataStore.stopReadingHeartRate(context, new HiRealTimeListener() {
            @Override
            public void onResult(int resultCode) {
                Log.i(TAG, "call doStopReadingHeartRate() resultCode is " + resultCode);
                Message message = Message.obtain();
                message.what = STOP_READING_HEARTRATE;
                message.obj = resultCode;
                //demoHandler.sendMessage(message);
            }

            @Override
            public void onChange(int resultCode, String data) {
                Log.i(TAG, "call doStopReadingHeartRate() resultCode is " + resultCode);
                Log.i(TAG, "call doStopReadingHeartRate() value is " + data);
                Message message = Message.obtain();
                message.what = STOP_READING_HEARTRATE;
                message.obj = data;
                //demoHandler.sendMessage(message);
            }
        });
    }
}
