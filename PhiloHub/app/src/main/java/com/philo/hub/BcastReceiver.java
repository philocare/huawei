package com.philo.hub;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.icu.util.Calendar;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;


import java.util.Objects;

import static android.content.Context.ALARM_SERVICE;
import static android.content.Context.JOB_SCHEDULER_SERVICE;


public class BcastReceiver extends BroadcastReceiver {

    private static JobScheduler jobScheduler;
    Context mCtx;
    private Notification note;

    private static final String TAG = BcastReceiver.class.getSimpleName();

    public void setContext(Context ctx) {
        mCtx = ctx;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();

        // 24-07-2020 - Robson
        // Por decisão do Douglas não vamos focar no serviço e o plicativo irá fazer a
        // extração de dados e exibição na tela
        //if (Objects.equals(action, "RESTART")) {
        //    Log.i(BcastReceiver.class.getSimpleName(), "Service Stops! Oooooooooooooppppssssss!!!!");
        //    context.startService(new Intent(context, GetDataService.class));
        //}

        if (Objects.equals(action, MainActivity.ACTION_MONITOR)) {
            note = new Notification(mCtx, null);
            note.send_note("Oi", "Philo Care Hub", "Estamos monitorando sua saúde!", R.drawable.ic_stat_philo, null);
        }
        /*Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(mCtx, notification);
        r.play();*/

        /*try {
            note = new Notification(mCtx, MainActivity.class);
            note.send_note("Oi", "Philo Care Hub", "Estamos monitorando sua saúde!", R.drawable.ic_stat_philo, null);
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(mCtx, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        /*
        final String action = intent.getAction();
        // an Intent broadcast.
        Toast.makeText(context, "Sincronização de dados.",
                Toast.LENGTH_LONG).show();*/

        /*try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(context, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        /*scheduleJob(context);

        // Vibrate the mobile phone
        //Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        //vibrator.vibrate(2000);

        mCtx = context;
        // faz bind com o serviço


        // Estou nesse ponto onde transferi todas as rotinas de comunicação para cá mas não tive sucesso
        // em fazer o bind com o serviço
        // Imagino que o bind terá de ser feito na main Activity e chamado pelo alarme de tempos em tempos
        // Fazer mais testes
        

        //if (action.equals(MainActivity.ACTION_SNOOZE)) {
        //    Toast.makeText(context, "Ação recebida de mensagem", LENGTH_LONG).show();
        //}
        //throw new UnsupportedOperationException("Not yet implemented");

        Intent intent_next = new Intent(context, BcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                context, 234324243, intent_next, 0);

        note = new Notification(context, MainActivity.class);
        note.send_note("Oi", "Philo Care Hub", "Estamos monitorando sua saúde!", R.drawable.ic_stat_philo, pendingIntent);


        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        //alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
        //        + (5 * 1000), pendingIntent);
        //Toast.makeText(this, "Alarm set in " + 5 + " seconds",
        //        Toast.LENGTH_LONG).show();

        // With setInexactRepeating(), you have to use one of the AlarmManager interval
        // constants--in this case, AlarmManager.INTERVAL_DAY.
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, 5 * 60 * 1000,
                AlarmManager.INTERVAL_FIFTEEN_MINUTES, pendingIntent);*/
    }

    public static void scheduleJob(Context context) {

        if (jobScheduler == null) {
            jobScheduler = (JobScheduler) context
                    .getSystemService(JOB_SCHEDULER_SERVICE);
        }
        ComponentName componentName = new ComponentName(context,
                JobService.class);
        JobInfo jobInfo = new JobInfo.Builder(1, componentName)
                // setOverrideDeadline runs it immediately - you must have at least one constraint
                // https://stackoverflow.com/questions/51064731/firing-jobservice-without-constraints
                .setOverrideDeadline(0)
                .setPersisted(true).build();
        jobScheduler.schedule(jobInfo);
    }



}
