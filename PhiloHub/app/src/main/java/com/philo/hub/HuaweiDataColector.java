/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package com.philo.hub;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import com.huawei.hihealth.listener.ResultCallback;
import com.huawei.hihealthkit.HiHealthDataQuery;
import com.huawei.hihealthkit.HiHealthDataQueryOption;
import com.huawei.hihealthkit.data.HiHealthSetData;
import com.huawei.hihealthkit.data.store.HiHealthDataStore;
import com.huawei.hihealthkit.data.type.HiHealthDataType;
import com.huawei.hihealthkit.data.type.HiHealthSetType;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.philo.hub.MainActivity.index_save;

public class HuaweiDataColector {

    private static final String TAG = "HuaweiDataColector";
    private static final String SPLIT = "*******************************" + System.lineSeparator();

    // TextView for displaying operation information on the UI
    private static TextView logInfoView;

    // Object of controller for fitness and health data, providing APIs for read/write, batch read/write, and listening
    //private static DataController dataController;

    private static String last_last_date = "";

    /**
     * TextView to send the operation result logs to the logcat and to the UI
     *
     * @param string (indicating the log string)
     */
    private static void logger(String string) {
        SysUtils.logger(string, TAG, logInfoView);
    }

    /**
     * Inicializa o data controler de extração
     * @param ctx - Contexto da aplicação
     * @param txtView - Objeto de exibição
     */
    public static void initDataController(Context ctx, TextView txtView ) {
        logInfoView = txtView;
    }

    public static void getHealthData(Context ctx) throws ParseException, IOException {

        // Controle de salvamentos diário
        index_save++;
        if (index_save > 9999) index_save = 0;

        SysUtils.saveInfoData(ctx, String.valueOf(index_save), "index");
        getDataFromHuawei(ctx, HiHealthSetType.DATA_SET_HEART, false);
        getDataFromHuawei(ctx, HiHealthSetType.DATA_SET_WALK_METADATA, false);
        getDataFromHuawei(ctx, HiHealthSetType.DATA_SET_CORE_SLEEP, false);
        //getDataFromHuawei(ctx, HiHealthSetType.HEART_CONTENT, false);

    }

    private static void getDataFromHuawei(final Context ctx, final int datatype, final boolean overwrite) throws ParseException {

        // 1. Build the time range for the query: start time and end time.
        // Pega a data salva salva
        String dateString = "";
        String last_timestamp = SysUtils.loadInfoData(ctx, String.valueOf(datatype));

        // preenche 3 dias de dados a partir de hoje backward
        last_timestamp = "NOT_FOUND";

        // se tem a data salva, grava como sendo a data atual a ser extraída
        if (!last_timestamp.equals("NOT_FOUND")) {
            // pega somente a data
            dateString = last_timestamp;
        } else {
            // 9 dias atrás
            Date today = new Date();
            // 9 dias em miliseguntos 2147483648 max int -- pode melhorar para o histórico!
            long day_minutes_millis = 2073600000; // 24 dias //30 * (24*(60*(60*1000)));
            // hoja menos 9 dias
            long initial_date = today.getTime() - day_minutes_millis;
            Date idate = new Date(initial_date);
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            dateString = dateFormat1.format(idate);
        }

        // transfoma a date em objeto
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

        // Para debug vamos fixar uma data inicial
        //Date startDate = dateFormat.parse("2021-01-12 09:00:00");
        //Date endDate = dateFormat.parse("2021-01-12 10:05:00");

        Date startDate = dateFormat.parse(dateString);

        // calcula a diferença com a data de hoje
        Date endDate = new Date();

        int timeout = 0;
        // o fim é o dia de hoje
        long endTime = System.currentTimeMillis();
        // o ultimo dia extraido
        long startTime = startDate.getTime(); // Check Data of the last 30 days

        HiHealthDataQuery hiHealthDataQuery = new HiHealthDataQuery(
                datatype,
                startTime, //<----- variable startTime used
                endTime,//<----- variable endTime used
                new HiHealthDataQueryOption()
        );

        HiHealthDataStore.execQuery(ctx, hiHealthDataQuery, timeout, new ResultCallback() {
            @Override
            public void onResult(int resultCode, Object data) {
                if (data != null) {
                    Log.i(TAG, "query not null");
                    List dataList = (List) data;
                    long lastDay = 0;

                    // verifica se teve sucesso
                    if ((resultCode == 0) && (dataList.size() > 0)) {
                        long last_day = 0;
                        logger("Sucesso lendo dados do HMS core. Tipo:" + datatype + "\n");
                        //stringBuffer.append("resultCode is ").append(String.valueOf(resultCode)).append(";(");
                        logger("Número de registros: " + dataList.size());
                        // Nesse ponto se constroí o JSON com os dados para enviar para o servidor
                        String dataJSON = SysUtils.buildJSONSampleSet(dataList);
                        // Se não tem nada
                        if (dataJSON.length() == 0) return;

                        int lindex = dataJSON.lastIndexOf("endtime");
                        // pega a data
                        String last_date = dataJSON.substring(lindex + 10, lindex + 29);
                        // salva a última data obtida
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String lastts = sdf.format(new Date(lastDay));
                        try {
                            // Salva o JSON
                            SysUtils.writeTxtToFile(MainActivity.user_id, "[" + dataJSON + "]\n", index_save, overwrite);                            SysUtils.saveInfoData(ctx, last_date, String.valueOf(datatype));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        logger(SPLIT);
                    }
                }
            }
        });
    }

    /*private static void getSleepDataFromHuawei(Date startDate, Date endDate, DataType datatype) {
        // Pega os batimentos
        ReadOptions readOptions = new ReadOptions.Builder().read(datatype)
                .setTimeRange(startDate.getTime(), endDate.getTime(), TimeUnit.MILLISECONDS)
                .build();

        // Create a data collector for sleep details.
        // SampleSet in ActivityRecord is used to bear the detailed data.
        DataCollector dataCollector = new com.huawei.hms.hihealth.data.DataCollector.Builder()
                .setDataType(DataType.DT_CONTINUOUS_SLEEP)
                .setDataGenerateType(DataCollector.DATA_TYPE_RAW)
                .setPackageName(mContext)
                .setDataCollectorName("test1")
                .build();

        // 3. Use the specified condition query object to call the data controller to query the sampling dataset.
        Task<ReadReply> readReplyTask = dataController.read(readOptions);

        // 4. Calling the data controller to query the sampling dataset is an asynchronous operation.
        // Therefore, a listener needs to be registered to monitor whether the data query is successful or not.
        readReplyTask.addOnSuccessListener(readReply -> {
            logger("Success read an SampleSets from HMS core");
            for (SampleSet sampleSet : readReply.getSampleSets()) {
                //showSampleSet(sampleSet);
                // Nesse ponto se constroí o JSON com os dados para enviar para o servidor
                String dataJSON = SysUtils.buildJSONSampleSet(sampleSet);
                // Salva arquivo
                SysUtils.writeTxtToFile("Ainda não", dataJSON, true, datatype.getName());
            }
            logger(SPLIT);
        }).addOnFailureListener(e -> SysUtils.printFailureMessage(TAG, e, "getHealthData", logInfoView));
    }*/

}
