/*
 * Aplicativo para extrair dados do Huawei "Health"
 * SDK HiHealth kit
 * Copyright (c) Philo Care do Brasil Ltda. Todos direitos reservados
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2019. All rights reserved.
 */

package hub.philo.com;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;

import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.huawei.hmf.tasks.Task;
import com.huawei.hms.hihealth.DataController;
import com.huawei.hms.hihealth.HiHealthOptions;
import com.huawei.hms.hihealth.HuaweiHiHealth;
import com.huawei.hms.hihealth.data.DataType;
import com.huawei.hms.hihealth.data.Field;
import com.huawei.hms.hihealth.data.SamplePoint;
import com.huawei.hms.hihealth.data.SampleSet;
import com.huawei.hms.hihealth.options.ReadOptions;
import com.huawei.hms.hihealth.result.ReadReply;
import com.huawei.hms.support.hwid.HuaweiIdAuthManager;
import com.huawei.hms.support.hwid.result.AuthHuaweiId;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private static final int DEFAULT_MAP_SIZE = 16;

    private static final int DEFAULT_LIST_SIZE = 16;

    private static final int DEFAULT_STRING_BUFFER_SIZE = 16;

    private static final int REQUEST_AUTHORIZATION = 0;

    private static final int PERMISSION_STATUS = 1;

    private static final int GET_GENDER = 2;

    private static final int GET_BIRTHDAY = 3;

    private static final int GET_HEIGHT = 4;

    private static final int GET_WEIGHT = 5;

    private static final int EXEC_QUERY = 6;

    private static final int GET_COUNT = 7;

    private static final int SAVE_SAMPLE = 8;

    private static final int SAVE_SAMPLES = 9;

    private static final int DELETE_SAMPLE = 10;

    private static final int DELETE_SAMPLES = 11;

    private static final int START_READING_HEARTRATE = 12;

    private static final int STOP_READING_HEARTRATE = 13;

    private static final int START_READING_RRI = 14;

    private static final int STOP_READING_RRI = 15;

    private static final int START_REALTIME_SPORT = 16;

    private static final int STOP_REALTIME_SPORT = 17;

    private static final int SEND_MSG_TO_DEVICE = 18;

    private TextView mTitleTextView;
    private TextView textConnection;
    private PopupWindow window;
    public int counter=0;

    private TextView tv1, tv2, tv3;
    private Message msg;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String s = msg.obj.toString();
//            Toast.makeText(mContext, s, Toast.LENGTH_LONG).show();

            switch (msg.what) {
                case 1:
                    tv1.setText(s + "\n");
                    break;
                case 2:
                    tv2.setText(s + "\n");
                    break;
                case 3:
                    tv3.setText(s + "\n");
                    break;
            }
        }
    };

    // serviço que será executado em background
    //private GetDataService mGetDataService;
    private TimerTask timerTask;
    private TimerTask postTimerTask;
    public int timer_counter=0;
    long oldTime=0;


    // TextView for displaying operation information on the UI
    private TextView logInfoView;

    private Context ctx;

    private Context mContext = MainActivity.this;

    public Context getCtx() {
        return ctx;
    }

    // Nome do canal de comunicação para mensagens no status bar
    public static final String CHANNEL_ID = "common";
    public static final String ACTION_SNOOZE = "snooze";
    public static final String ACTION_MONITOR = "monitoring_action";
    private Notification note;

    private void sendMsg(String message, int what) {
        msg = Message.obtain();
        msg.what = what;
        msg.obj = message;
        mHandler.sendMessage(msg);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ctx = this;

        logInfoView = (TextView) findViewById(R.id.data_controller_log_info);
        logInfoView.setMovementMethod(ScrollingMovementMethod.getInstance());

        // Exibição de textos
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);

        requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,READ_EXTERNAL_STORAGE}, 1);

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //popSplashWindow(findViewById(R.id.main_layout), R.layout.activity_splash);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        View main_view = (View)findViewById(R.id.main_layout);
                        main_view.setVisibility(View.VISIBLE);
                        //dismissSplashWindow();
                    }
                }, 2000);
            }
        }, 100);*/

        // prepara a tela de scaneamento inicial
        initRecyleView();

        /*
        Notificações no status bar
        */
        createNotificationChannel();

        // Verifica a permissão do aplicativo
        //doGetGender(getCtx());

        /*mGetDataServiceIntent = new Intent(this, GetDataService.class);
        mGetDataServiceIntent.putExtra("inputExtra", "Foreground Service Example in Android");
        ContextCompat.startForegroundService(this, mGetDataServiceIntent);*/

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_MONITOR);
        filter.addAction(ACTION_SNOOZE);

        // serviços de background
        //Intent mGetDataServiceIntent;
        //Intent serviceIntent;
        BcastReceiver receiver = new BcastReceiver();
        receiver.setContext(getCtx());
        registerReceiver(receiver, filter);

        // 24-07-2020 - Robson
        // Por decisão do Douglas não vamos focar no serviço e o plicativo irá fazer a
        // extração de dados e exibição na tela
        // Cria o serviço "imortal" de busca de dados
        //mGetDataService = new GetDataService();
        //mGetDataService.setCtx(getCtx());
        //mGetDataServiceIntent = new Intent(getCtx(), mGetDataService.getClass());
        //startService(mGetDataServiceIntent);
        /*
        if (!isMyServiceRunning(mGetDataService.getClass())) {
            //startService(mGetDataServiceIntent);
        }
         */

        /*
        Notificações no status bar
        */
        createNotificationChannel();

        // inicia o timer resposável pela extração de dados
        //startTimer();

        // inicializa o controlador dos dados
        HuaweiDataColector.initDataController(mContext, logInfoView);
    }



    private void initRecyleView() {
        //mSwipeRefreshLayout = (SwipeRefreshLayout) super.findViewById(R.id.mian_swipeRefreshLayout);
        //mRecyclerView = (RecyclerView) super.findViewById(R.id.main_recylerlist);
        mTitleTextView = (TextView) super.findViewById(R.id.main_title);
        mTitleTextView.setText(getString(R.string.app_name) + " " + getAppVersion(mContext));
        //mInfoTextView = (TextView) super.findViewById(R.id.textViewInfo);
    }

    /**
     Inicializa o timer de extração de dados do Huawei Health
     */
    public void startTimer() {
        //set a new Timer
        /**
         Variáveis para o timer de extração de dados
         */
        Timer timer = new Timer();
        Timer postTimer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask(getCtx());

        // inicializa o timer de post
        initializePostTimerTask();

        // Mudar para 10 minuots porque o mínimo do W74 é 5 minutos
        timer.schedule(timerTask, 5000, 10 * 60 * 1000); //
        postTimer.schedule(postTimerTask, 5000, 5 * 60 * 1000); //
    }
    /**
     * Inicializa a tarefa (thread) que faz o serviço de extração e calculos
     */
    public void initializeTimerTask(Context ctx) {
        timerTask = new TimerTask() {
            public void run() {
                // extraí os dados do aplicativo Saúde
                try {
                    HuaweiDataColector.getHealthData(mContext);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    /**
     * Inicializa a tarefa (thread) que faz o serviço de conexão da pulseira
     */
    public void initializePostTimerTask() {
        postTimerTask = new TimerTask() {
            public void run() {
                Log.i("in timer", "in timer ++++  "+ (timer_counter++));
                try {
                    //HideBars();
                    postAndGetJsonWebToken();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    /**
     * Envia dados para a servidor na nuvem
     */
    private void postAndGetJsonWebToken() {
        //HttpURLConnection conn = null;

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try  {
                    //Your code goes here
                    String response = "";

                    // Dados para autenticação
                    StringBuilder sbParams = new StringBuilder();
                    sbParams.append("{");
                    sbParams.append("\"passwd\"");
                    sbParams.append(":");
                    sbParams.append("\"123456aA\"");
                    sbParams.append(",");
                    sbParams.append("\"email\"");
                    sbParams.append(":");
                    sbParams.append("\"philo\"");
                    sbParams.append("}@");

                    // Dados extraidos da pulseira
                    // Lê um arquivo e envia no post
                    String[] samples = SysUtils.readTxtFile();

                    if (samples == null) {
                        sendMsg("Nada para enviar", 1);
                        return;
                    }

                    sbParams.append(samples[1]);


                    try{
                        //String requestURL = "https://www.philocare.com/engine/devicedatareceive_post.php";
                        String requestURL = "https://philo.solutions/engine/huawei_devicedatareceive_post.php";

                        URL url;

                        try {
                            url = new URL(requestURL);

                            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            conn.setReadTimeout(15000);
                            conn.setConnectTimeout(15000);
                            conn.setRequestMethod("POST");
                            conn.setDoInput(true);
                            conn.setDoOutput(true);


                            OutputStream os = conn.getOutputStream();
                            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(os, "UTF-8"));
                            writer.write(sbParams.toString());

                            writer.flush();
                            writer.close();
                            os.close();
                            int responseCode=conn.getResponseCode();

                            if (responseCode == HttpsURLConnection.HTTP_OK) {
                                String line;
                                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                                while ((line=br.readLine()) != null) {
                                    response+=line;
                                }
                            }
                            else {
                                response="";
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        //return response;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String millisInString  = dateFormat.format(new Date());

                    sendMsg(millisInString + ": " + response, 2);
                    // apaga o arquivo enviado
                    if (response.contains("processed")) {
                        SysUtils.deleteFile(samples[0]);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        thread.start();

    }

    /**
     * Verifica se já possui ultima leitura (o registro salvo de HR)
     * Sem registro significa que será a primeira vez que está executando
     */
    private boolean getLastHRRead() {
        // verifica se o arquivo existe
        String res = SysUtils.getHRSavedDate(getCtx());
        if (res.equals(SysUtils.NOT_FOUND)) {
            return false;
        } else {
            return true;
        }
    }
    /**
     * Verifica se já possui ultima leitura (o registro salvo de Sleep)
     * Sem registro significa que será a primeira vez que está executando
     */
    private boolean getLastSleepRead() {
        // verifica se o arquivo existe
        String res = SysUtils.getSleepSavedDate(getCtx());
        if (res.equals(SysUtils.NOT_FOUND)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Notificação de monitoramento
     */
    private void showMonitorNote() {
        Intent intent = new Intent(this, BcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                this.getApplicationContext(), 234324243, intent, 0);

        note = new Notification(this, MainActivity.class);
        note.send_note("Oi", "Philo Care Hub", "Estamos monitorando sua saúde!", R.drawable.ic_stat_philo, pendingIntent);
    }

    // 24-07-2020 - Robson
    // Por decisão do Douglas não vamos focar no serviço e o plicativo irá fazer a
    // extração de dados e exibição na tela
    /**
    Verifica se o serviço está rodadando
     */
    /*private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }*/

    //#############################################################################################
    // Funções executadas depois do oncreate
    //#############################################################################################
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume () {
        super.onResume();
        // usaremos a função getGender para verificar se estamos conseguido ler o Huawei
        //doGetGender(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }


    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "onStop");

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
        // 24-07-2020 - Robson
        // Por decisão do Douglas não vamos focar no serviço e o plicativo irá fazer a
        // extração de dados e exibição na tela
        //sendBroadcast(new Intent("RESTART"));
        //stopService(mGetDataServiceIntent);
    }


    /**
    Cria o canal de comunicação do aplicativo
    */
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }
    }

    /**
    Desativa a tela de splash
     */
    public void dismissSplashWindow() {
        if (window != null) {
            window.dismiss();
            window = null;

        }

    }

    /**
    Exibe a tela de splash
     */
    public void popSplashWindow(View parent, int windowRes) {
        if (window == null) {
            LayoutInflater lay = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert lay != null;
            View popView = lay.inflate(windowRes, null);
            popView.setAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_out));

            window = new PopupWindow(popView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, true);
            window.setOutsideTouchable(true);
            window.setFocusable(true);
            window.update();

            window.showAtLocation(parent, Gravity.CENTER_VERTICAL, 0, 0);
            window.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    //bStart = true;
                    //callRemoteScanDevice();
                    window = null;
                }
            });

            // Evento de toque na tela
            /*window.setTouchInterceptor(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    dismissSplashWindow();
                    return false;
                }
            });*/

        }
    }

    //#############################################################################################
    // AÇÕES DE BOTÕES
    //#############################################################################################


    /**
     * ###################################################################################
     * Captura dados da pulseira
     * ###################################################################################
     */

    public void getDeviceData(View view) throws ParseException {
        // busca os dados
        //doGetHRData();
        //doExecQueryHR30Days(getCtx());
        //doGetCount(getCtx());
    }

    public void sendPost(View view) {
        postAndGetJsonWebToken();
    }

    /**
    Envia um broadcast para o receiver
     */
    public void btnBroadcastClick(View view) {

        Intent intent = new Intent(this, BcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                this.getApplicationContext(), 234324243, intent, 0);

        note = new Notification(this, MainActivity.class);
        note.send_note("Oi", "Philo Care Hub", "Estamos monitorando sua saúde!", R.drawable.ic_stat_philo, pendingIntent);


        /*
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        assert alarmManager != null;
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                + (5 * 1000), pendingIntent);
        Toast.makeText(this, "Alarm set in " + 5 + " seconds",
                Toast.LENGTH_LONG).show();
         */
    }

    /**
     * Faz login no aplicativo Saúde da Huawei
     * e Ativa a tela de configuração
    */
    public void btnSettingsClick(View view) {
        Intent intent = new Intent(this, HealthKitAuthClientActivity.class);
        startActivity(intent);
    }

    /**
     * Use the data controller to query the sampling dataset by specific criteria.
     *
     * @param view (indicating a UI object)
     * @throws ParseException (indicating a failure to parse the time string)
     */
    public void readData(View view) throws ParseException {

        // busca dados do Saúde
        HuaweiDataColector.getHealthData(mContext);
    }

    /**
     * Print the SamplePoint in the SampleSet object as an output.
     *
     * @param sampleSet (indicating the sampling dataset)
     */
    private void showSampleSet(SampleSet sampleSet) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        for (SamplePoint samplePoint : sampleSet.getSamplePoints()) {
            logger("Sample point type: " + samplePoint.getDataType().getName());
            logger("Start: " + dateFormat.format(new Date(samplePoint.getStartTime(TimeUnit.MILLISECONDS))));
            logger("End: " + dateFormat.format(new Date(samplePoint.getEndTime(TimeUnit.MILLISECONDS))));
            for (Field field : samplePoint.getDataType().getFields()) {
                logger("Field: " + field.getName() + " Value: " + samplePoint.getFieldValue(field));
            }
        }
    }

    /**
     * TextView to send the operation result logs to the logcat and to the UI
     *
     * @param string (indicating the log string)
     */
    private void logger(String string) {
        SysUtils.logger(string, TAG, logInfoView);
    }

    private void doGetLastDate(Context context) {
        String last_hr_date;
        String last_sleep_date;

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        last_hr_date = SysUtils.getHRSavedDate(context);
        last_sleep_date = SysUtils.getSleepSavedDate(context);

        String macAddress = android.provider.Settings.Secure.getString(context.getContentResolver(), "bluetooth_address");

        sendMsg("Última data HR:" + last_hr_date + " Última data Sleep: " + last_sleep_date +
                " BlueMAC: " + bluetoothAdapter.getAddress(), 1);
    }

    public String getAppVersion(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            String version = info.versionName;
            return version;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
